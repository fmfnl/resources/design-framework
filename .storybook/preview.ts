import { type Preview, type WebComponentsRenderer, setCustomElementsManifest } from "@storybook/web-components";
import { withThemeByClassName } from "@storybook/addon-themes";

// Include base styles we need available for every component
import "../src/index.css";
import '@fortawesome/fontawesome-free/css/all.css';

// Generate metadata for our web components
import manifest from "../custom-elements.json";
setCustomElementsManifest(manifest);

const preview: Preview = {
    decorators: [
        withThemeByClassName<WebComponentsRenderer>({
            themes: {
                "Light": "",
                "Dark": "dark",
            },
            defaultTheme: "Light"
        })
    ],
    parameters: {
        actions: { argTypesRegex: "^on[A-Z].*" },
        controls: {
            matchers: {
                color: /(background|color)$/i,
                date: /Date$/,
            },
        },
    },
};

export default preview;
