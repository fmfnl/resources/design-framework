import { create } from "@storybook/theming/create";
import fmfLogo from "../src/assets/fmf.svg";

export default create({
    base: "light",
    brandTitle: "FMF Components",
    brandUrl: "https://fmf.nl/",
    brandImage: fmfLogo,
    brandTarget: "_self",

    barSelectedColor: "#173494",
})
