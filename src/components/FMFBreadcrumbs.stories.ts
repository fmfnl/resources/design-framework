import { Meta, StoryObj } from "@storybook/web-components";
import { FMFBreadcrumbs, BreadcrumbItem } from "./FMFBreadcrumbs";
import "./FMFBreadcrumbs";

const meta = {
    component: "fmf-breadcrumbs",
    title: "Page Building Blocks/Breadcrumbs",
    tags: [ "autodocs" ],
} satisfies Meta<FMFBreadcrumbs>;
export default meta;

type Story = StoryObj<FMFBreadcrumbs>;
export const Default = {
    args: {
        breadcrumbs: [
            new BreadcrumbItem("Test1", "www.fmf.nl"),
            new BreadcrumbItem("Test2", "www.fmf.nl", "fa-solid fa-book-skull"),
            new BreadcrumbItem("Test3", "www.fmf.nl")
        ]
    }
} satisfies Story;