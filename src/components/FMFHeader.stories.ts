import { Meta, StoryObj } from "@storybook/web-components";
import { FMFHeader, HeaderItem, HeaderMenu } from "./FMFHeader";

const meta = {
    component: "fmf-header",
    title: "Page Building Blocks/Header",
    tags: [ "autodocs" ],
} satisfies Meta<FMFHeader>;
export default meta;

type Story = StoryObj<FMFHeader>;
export const Basic = {
    args: {
        title: "Foo",
        home: "/",
        items: [
            new HeaderMenu(
                "Menu", "fa-stream", "", true, [
                    new HeaderItem("Item 1-1", "", "#"),
                    new HeaderItem("Item 1-2", "", "#"),
                ]
            ),
            new HeaderItem(
                "News", "fa-bell", "https://www.fmf.nl/a/news/",
            ),
            new HeaderItem(
                "Calendar", "fa-calendar-alt", "https://www.fmf.nl/a/calendar",
            ),
            new HeaderItem(
                "Association", "fa-people-group", "https://www.fmf.nl/vereniging",
            )
        ]
    }
} satisfies Story;
