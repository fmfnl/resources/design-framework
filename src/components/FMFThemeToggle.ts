import { customElement } from "lit/decorators.js";
import BaseElement from "./BaseElement";
import { PropertyValueMap, html } from "lit";

/**
 * Dark mode toggle. The logic is as follows:
 * - By default, the theme will dynamically follow browser preference
 * - If the user presses the toggle, we will override the current theme and
 *   set a new one in localStorage.
 * - However, if the user toggles the theme back to <whatever the browser
 *   preference is>, then we remove the override from localStorage and return
 *   to dynamic behaviour that follows the browser.
 * 
 * This approach allows us to return to dynamic behaviour easily.
 */
@customElement("fmf-theme-toggle")
export class FMFThemeToggle extends BaseElement {
    private isDark: boolean = false; // Light theme by default
    private isBrowserDark: MediaQueryList;

    constructor() {
        super();

        this.isBrowserDark = window.matchMedia("(prefers-color-scheme: dark)");
    }

    protected firstUpdated(_changedProperties: PropertyValueMap<any> | Map<PropertyKey, unknown>): void {
        this.isBrowserDark.addEventListener("change", event => {
            if(!("theme" in localStorage)) {
                this.isDark = event.matches;
                this.updateTheme();
            }
        })
    }

    protected handleThemeToggle() {
        if(!("theme" in localStorage)) {
            // If no preference is set, we set a new preference opposite of current browser setting
            this.isDark = !this.isBrowserDark.matches;
            localStorage.setItem("theme", this.isDark ? "dark" : "light");
        } else {
            // We have a preference set - we toggle the preference. If it is then equal to the browser
            // setting, we remove the setting.
            this.isDark = localStorage.getItem("theme") !== "dark";
            if(this.isDark === this.isBrowserDark.matches) {
                localStorage.removeItem("theme");
            } else {
                localStorage.setItem("theme", this.isDark ? "dark" : "light");
            }
        }

        this.updateTheme();
    }

    protected updateTheme() {
        // Assume this.isDark is set correctly, update UI
        if(this.isDark) {
            document.documentElement.classList.add("dark");
        } else {
            document.documentElement.classList.remove("dark");
        }
    }

    protected render() {
        return html`
            <button
                id="theme-toggle"
                type="button"
                class="p-1 bg-gray-200 dark:bg-gray-800 rounded-full text-center text-gray-500 dark:text-gray-400 hover:bg-gray-100 dark:hover:bg-gray-700"
                @click="${this.handleThemeToggle}"
            >
                <svg
                    id="theme-toggle-dark-icon"
                    fill="currentColor"
                    viewBox="0 0 20 20"
                    xmlns="http://www.w3.org/2000/svg"
                    class="w-6 h-6 dark:hidden"
                    width="1em"
                    height="1em"
                >
                    <path d="M17.293 13.293A8 8 0 016.707 2.707a8.001 8.001 0 1010.586 10.586z"></path>
                </svg>
                <svg
                    id="theme-toggle-light-icon"
                    class="w-6 h-6 hidden dark:block"
                    fill="currentColor"
                    viewBox="0 0 20 20"
                    xmlns="http://www.w3.org/2000/svg"
                    width="1em"
                    height="1em"
                >
                    <path
                        d="M10 2a1 1 0 011 1v1a1 1 0 11-2 0V3a1 1 0 011-1zm4 8a4 4 0 11-8 0 4 4 0 018 0zm-.464 4.95l.707.707a1 1 0 001.414-1.414l-.707-.707a1 1 0 00-1.414 1.414zm2.12-10.607a1 1 0 010 1.414l-.706.707a1 1 0 11-1.414-1.414l.707-.707a1 1 0 011.414 0zM17 11a1 1 0 100-2h-1a1 1 0 100 2h1zm-7 4a1 1 0 011 1v1a1 1 0 11-2 0v-1a1 1 0 011-1zM5.05 6.464A1 1 0 106.465 5.05l-.708-.707a1 1 0 00-1.414 1.414l.707.707zm1.414 8.486l-.707.707a1 1 0 01-1.414-1.414l.707-.707a1 1 0 011.414 1.414zM4 11a1 1 0 100-2H3a1 1 0 000 2h1z"
                        fill-rule="evenodd"
                        clip-rule="evenodd"
                    ></path>
                </svg>
            </button>
        `;
    }
}

declare global {
    interface HTMLElementTagNameMap {
        "fmf-theme-toggle": FMFThemeToggle;
    }
}
