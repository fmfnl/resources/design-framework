import { customElement } from "lit/decorators.js";
import BaseElement from "./BaseElement";
import { html } from "lit";

@customElement("fmf-app-container")
export class FMFAppContainer extends BaseElement {
    protected render() {
        return html`
            <div class="flex flex-col bg-white text-[#333333] dark:bg-gray-700 dark:text-[#ffffff]">
                ${this.children}
            </div>
        `;
    }
}

declare global {
    interface HTMLElementTagNameMap {
        "fmf-app-container": FMFAppContainer;
    }
}
