import { customElement, property } from "lit/decorators.js";
import BaseElement from "./BaseElement";
import { html } from "lit";
import FMFLogo from "../assets/fmf.svg?raw";
import wave from "../assets/wave.svg?raw";
import { repeat } from "lit/directives/repeat.js";
import { unsafeSVG } from "lit/directives/unsafe-svg.js";

@customElement("fmf-footer")
export class FMFFooter extends BaseElement {
    @property({ type: String })
    public repository: string = "https://gitlab.com/fmfnl";

    @property({ type: Array })
    public columns: Array<FooterColumn> = [
        new FooterColumn("Learn", [
            new FooterItem("Practice Sessions", "https://practicesessions.nl/"),
            new FooterItem("Book Sales", "https://fmf.nl/studie/boekenbestelling"),
            new FooterItem("Studies", "https://fmf.nl/studie"),
        ]),
        new FooterColumn("Other", [
            new FooterItem("Join FMF", "https://fmf.nl/join"),
            new FooterItem("MyFMF", "https://fmf.nl/mijnfmf"),
            new FooterItem("Calendar", "https://fmf.nl/a/calendar"),
        ]),
        new FooterColumn("Follow Us!", [
            new FooterItem("Instagram", "https://instagram.com/fmf_groningen?igshid=YmMyMTA2M2Y=", "fa-instagram"),
            new FooterItem("Facebook", "https://www.facebook.com/FMFGroningen", "fa-facebook"),
            new FooterItem("LinkedIn", "https://www.linkedin.com/company/fysisch-mathematische-faculteitsvereniging/", "fa-linkedin"),
            new FooterItem("WhatsApp", "https://chat.whatsapp.com/B7tQTamoMvV7C4vuvPOgJy", "fa-whatsapp"),
            ], true, 
        ),
        new FooterColumn("Legal", [
            new FooterItem("Privacy Policy", "https://fmf.nl/admin/media/get_file/2390"),
            new FooterItem("Terms & Conditions", "https://fmf.nl/admin/media/get_file/2395"),
            new FooterItem("Other Documents", "https://fmf.nl/vereniging/legal_documents"),
        ]),
    ];
      
    @property({ type: Number })
    public type: FooterType = FooterType.Fadeout;

    protected render() {

        return html`
            <footer class="py-4">
                ${this.type === FooterType.Wave ? html`
                    <div class="w-full bg-repeat-x text-fmf">
                    ${unsafeSVG(wave)}
                    </div>
                `: this.type == FooterType.Fadeout ? html`
                    <div class="h-[4vh] bg-gradient-to-b from-white to-fmf"></div>
                 ` : ""}
                <div class="mx-auto w-full h-max-full p-12 py-6 lg:py-8 bg-fmf">
                    <div class="md:flex md:justify-between">
                        <div class="mb-6 md:mb-0 md:w-1/3 flex-grow p-5">
                            <a href="https://www.fmf.nl/" class="flex px-4 text-white max-h-24 w-auto h-[80%]">
                               ${unsafeSVG(FMFLogo)}
                            </a>
                        </div>
                        <div class="text-[1rem] grid gap-20 grid-cols-2 sm:gap-6 sm:grid-cols-4 w-full">
                            ${repeat(this.columns,
                                (col) => col.name,
                                (col) => html`
                                    <div class="">
                                        <h2 class="mb-6 text-sm font-bold uppercase text-white">${col.name}</h2>
                                        <ul class=${col.isFlex ? "flex" : "static"}>
                                            ${repeat(col.items,
                                                (item) => item.name,
                                                (item) => html`
                                                    <li class="mb-3 sm:mb-2 flex flex-auto">
                                                        ${col.name == "Follow Us!" ? html`
                                                            <a href="${item.url}" class="inline-block text-[1.2rem] px-[0.1rem] transition-all hover:-mt-2 hover:pb-2 text-white">
                                                                ${item.icon ? html`<i class="fab ${item.icon} "></i>` : ""}
                                                            </a>
                                                        ` : html`
                                                            <a href="${item.url}" class="inline-block py-0.5 w-auto">
                                                                <span class="whitespace-nowrap w-auto text-white font-light text-opacity-80 hover:text-opacity-100 hover:font-semibold xl:hover:px-3 transition-all ease-in-out">
                                                                    ${item.icon ? html`<i class="fab ${item.icon}"></i>` : ""}
                                                                    ${item.name}
                                                                </span>
                                                            </a>
                                                        `}
                                                    </li>
                                                    
                                                `
                                            )}
                                        </ul>
                                        ${col.name == "Follow Us!" ? html` 
                                            <ul class="text-white font-medium text-sm sm:text-xs py-2">
                                                <li>Email us at: <a class="font-bold" href="mailto:board@fmf.nl">board@fmf.nl</a></li>
                                            </ul>
                                            `: ""
                                        }
                                    </div>
                                `
                            )}
                        </div>
                    </div>
                    <div class="items-center text-center py-5">
                        <span class="text-xs text-white font-[200]">
                            Fysisch-Mathematische Faculteitsvereniging
                            <p>
                                <a href=${this.repository}>
                                    <span class="hover:font-[400]">
                                        <i style="color: rgba(255, 255, 255, 0.8)" class="fa-brands fa-gitlab"></i> 
                                        Source Code
                                    </span>
                                </a>
                            </p>
                        </span>
                    </div>
                </div>
            </footer>
        `;
    }
}
//w-[10rem] lg:w-max xl:w-max
declare global {
    interface HTMLElementTagNameMap {
        "fmf-footer": FMFFooter;
    }
}

export enum FooterType {
    None = 0,
    Wave = 1,
    Fadeout = 2
  }

export class FooterColumn {
    constructor(
        public name: string,
        public items: Array<FooterItem> = [],
        public isFlex: boolean = false, 
    ) {}
}


export class FooterItem {
    constructor(
        public name: string,
        public url: string,
        public icon?: string,
    ) {}
}
