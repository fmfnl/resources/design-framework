import { Meta, StoryObj } from "@storybook/web-components";
import { FMFFooter, FooterColumn, FooterItem } from "./FMFFooter";

const meta = {
    component: "fmf-footer",
    title: "Page Building Blocks/Footer",
    tags: [ "autodocs" ],
} satisfies Meta<FMFFooter>;
export default meta;

type Story = StoryObj<FMFFooter>;
export const Standard = {
    args: {
        columns: [
            new FooterColumn("Learn", [
                new FooterItem("Practice Sessions", "https://practicesessions.nl/"),
                new FooterItem("Book Sales", "https://fmf.nl/studie/boekenbestelling"),
                new FooterItem("Studies", "https://fmf.nl/studie"),
            ]),
            new FooterColumn("Other", [
                new FooterItem("Join FMF", "https://fmf.nl/join"),
                new FooterItem("MyFMF", "https://fmf.nl/mijnfmf"),
                new FooterItem("Calendar", "https://fmf.nl/a/calendar"),
            ]),
            new FooterColumn("Follow Us!", [
                new FooterItem("Instagram", "https://instagram.com/fmf_groningen?igshid=YmMyMTA2M2Y=", "fa-instagram"),
                new FooterItem("Facebook", "https://www.facebook.com/FMFGroningen", "fa-facebook"),
                new FooterItem("LinkedIn", "https://www.linkedin.com/company/fysisch-mathematische-faculteitsvereniging/", "fa-linkedin"),
                new FooterItem("WhatsApp", "https://chat.whatsapp.com/B7tQTamoMvV7C4vuvPOgJy", "fa-whatsapp"),
            ], true),
            new FooterColumn("Legal", [
                new FooterItem("Privacy Policy", "https://fmf.nl/admin/media/get_file/2390"),
                new FooterItem("Terms & Conditions", "https://fmf.nl/admin/media/get_file/2395"),
                new FooterItem("Other Documents", "https://fmf.nl/vereniging/legal_documents"),
            ]),
        ]
    }
} satisfies Story;
