import { Meta, StoryObj } from "@storybook/web-components";
import { FMFAppContainer } from "./FMFAppContainer";
import  "./FMFHeader"
import "./FMFFooter"
import "./FMFAppContainer";

const meta = {
    component: "fmf-app-container",
    title: "Page Building Blocks/App Container",
    tags: [ "autodocs" ],
} satisfies Meta<FMFAppContainer>;
export default meta;

type Story = StoryObj<FMFAppContainer>;
export const Default = {
    args: {
        innerHTML: '<div><fmf-header></fmf-header>Test the inner content<fmf-footer></fmf-footer></div>',
    }
} satisfies Story;