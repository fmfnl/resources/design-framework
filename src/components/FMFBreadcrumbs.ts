import { customElement, property } from "lit/decorators.js";
import { repeat } from "lit/directives/repeat.js";
import BaseElement from "./BaseElement";
import { html } from "lit";

@customElement("fmf-breadcrumbs")
export class FMFBreadcrumbs extends BaseElement {
    @property({ type: Array })
    public breadcrumbs: Array<BreadcrumbItem> = [
        new BreadcrumbItem("Test1", "www.fmf.nl"),
        new BreadcrumbItem("Test2", "www.fmf.nl"),
    ];

    @property({ type: String })
    public home: string = "www.fmf.nl";

    protected render() {
        return html`
            <nav class="flex py-2">
                <ol class="inline-flex justify-stretch items-center">
                    ${repeat(
                        [new BreadcrumbItem("Home", this.home, "fa-solid fa-house-chimney"), ...this.breadcrumbs],
                        (item) => item.name,
                        (item) => html`
                            <li>
                                <div class="flex items-center">
                                    <!-- Chevron -->
                                    ${item.url === this.home && item.name == "Home" ? "" : html`
                                        <svg class="w-3 h-3 grow-0 text-gray-400" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 6 10">
                                            <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="m1 9 4-4-4-4"/>
                                        </svg>
                                    `}

                                    <!-- Navigation item itself -->
                                    <a href="${item.url}" class="text-gray-700 text-opacity-80 hover:text-opacity-100 dark:text-fmf mx-2 font-medium md:mx-4 transition-all ease-in-out hover:-mt-2 hover:pb-2">
                                        ${item.icon ? html`<i class="${item.icon} mr-2"></i>` : ""}
                                        <span class="text-sm inline-block font-medium">${item.name}</span>
                                    </a>
                                </div>
                            </li>
                        `
                    )}
                </ol>
            </nav>
        `;
    }
}

declare global {
    interface HTMLElementTagNameMap {
        "fmf-breadcrumbs": FMFBreadcrumbs;
    }
}

export class BreadcrumbItem {
    constructor(
        public name: string,
        public url: string,
        public icon?: string
    ) {}
}
