import { PropertyValueMap, html } from "lit";
import { customElement, property } from "lit/decorators.js";
import BaseElement from "./BaseElement";
import FMFLogo from "../assets/fmf.svg?raw";
import { repeat } from "lit/directives/repeat.js";
import { initDropdowns } from "flowbite";
import { unsafeSVG } from "lit/directives/unsafe-svg.js";

@customElement("fmf-header")
export class FMFHeader extends BaseElement {
    /**
     * Title of the application, to show next to the FMF logo
     */
    @property({ type: String })
    public title: string = "FMF";

    @property({ type: String })
    public home: string = "";

    @property({ type: Array })
    public items: Array<HeaderItem> = [];

    protected updated(_changedProperties: PropertyValueMap<any> | Map<PropertyKey, unknown>): void {
        initDropdowns();
    }

    protected render() {
        return html`
        <nav class=" border-gray-200 dark:bg-fmf-dark">
            <div class="flex flex-wrap items-center justify-between max-w-screen-xl mx-auto p-4">
                <a href=${this.home} class="flex fmf-items-center">
                    <span class="px-2 mr-2 border-r border-gray-800">
                        <div class="w-9 h-9 -mt-1 flex mx-auto rounded-full text-white bg-fmf ">
                            ${unsafeSVG(FMFLogo)}
                        </div>
                    </span>
          
                    <span class="self-center text-xl font-semibold whitespace-nowrap dark:text-white">${this.title}</span>
                </a>
                <div class="flex items-center md:order-2">
                    <a href="https://www.fmf.nl/mijnfmf" class="w-10 relative mr-2 cursor-pointer hover:text-gray-700 dark:hover:text-white">
                        <i class="w-8 fas fa-user p-2 dark:text-white bg-gray-200 dark:bg-gray-800 rounded-full text-center"></i>
                    </a> 
                    <span class="w-10 relative mr-2 cursor-pointer hover:text-gray-700 dark:hover:text-white">
                        <fmf-theme-toggle></fmf-theme-toggle>
                    </span>
                    <button data-collapse-toggle="mega-menu-icons" type="button" class="inline-flex items-center p-2 w-10 h-10 justify-center text-sm text-gray-500 rounded-lg md:hidden hover:bg-gray-100 focus:outline-none focus:ring-2 focus:ring-gray-200 dark:text-gray-400 dark:hover:bg-gray-700 dark:focus:ring-gray-600" aria-controls="mega-menu-icons" aria-expanded="false">
                        <span class="sr-only">Open main menu</span>
                        <svg class="w-5 h-5" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 17 14">
                            <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M1 1h15M1 7h15M1 13h15"/>
                        </svg>
                    </button>
                </div>
                <div id="mega-menu-icons" class="items-center justify-between hidden w-full md:flex md:w-auto md:order-1">
                    <ul class="flex flex-col mt-4 self-center items-center font-medium lg:flex-row lg:space-x-8 lg:mt-0">
                        <!--Menus -->
                        ${repeat(this.items,
                            (menu, index) => menu.label + index, // Use index to make IDs unique
                            (menu) => html`
                                <li class="px-2 py-1 cursor-pointer hover:bg-gray-200 hover:text-gray-700 dark:hover:bg-gray-800 dark:text-gray-300 dark:hover:text-white text-sm rounded">
                                    <!--If menu == true then dropdown created otherwise  menu is only a link -->
                                    ${(menu instanceof HeaderMenu && menu.hasItems) ? html`
                                        <button id="dropdownDelayButton-${menu.label}" data-dropdown-toggle="dropdownDelay-${menu.label}" data-dropdown-delay="50" data-dropdown-trigger="hover" type="button">
                                            <i class="w-8 fas ${menu.iconName} p-2 bg-gray-200 dark:text-white dark:bg-gray-600 rounded-full text-center"></i>
                                            ${menu.label}
                                        </button>
                                        <!-- Dropdown menu -->
                                        <div id="dropdownDelay-${menu.label}" class="absolute hidden pt-5 !-mt-5">
                                            <div class="w-60 px-5 py-3 dark:bg-gray-800 bg-white rounded-lg shadow border dark:border-transparent z-[10000]">
                                                <ul class="space-y-3 dark:text-white">
                                                    ${repeat(menu.items,
                                                        (item) => item.label,
                                                        (item) => html`
                                                            <li class="font-medium">
                                                                <a href=${item.url} class="flex items-center transform transition-colors duration-200 border-r-4 border-transparent hover:border-fmf-dark">
                                                                    <div class="mr-3">
                                                                        <i class="fa-solid ${(item.iconName === "") ? 'fa-arrow-right' : item.iconName} w-6 h-6 dark:text-white text-fmf-dark"></i>
                                                                    </div>
                                                                    ${item.label}
                                                                </a>
                                                            </li>
                                                        `
                                                    )}
                                                    <!--home link in dropdown -->
                                                    ${(menu.isHome) ? html`
                                                        <hr class="dark:border-gray-600">
                                                        <li class="font-medium">
                                                            <a href=${this.home} class="hover:!border-red-600 flex items-center transform transition-colors duration-200 border-r-4 border-transparent hover:border-fmf-dark">
                                                                <div class="mr-3 text-red-600">
                                                                    <svg class="w-6 h-6" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M17 16l4-4m0 0l-4-4m4 4H7m6 4v1a3 3 0 01-3 3H6a3 3 0 01-3-3V7a3 3 0 013-3h4a3 3 0 013 3v1"></path></svg>
                                                                </div>
                                                                Home
                                                            </a>
                                                        </li>
                                                        ` : ''
                                                    }
                                                </ul>
                                            </div>
                                        </div>
                                        <!-- end Dropdown menu -->
                                    ` : html`
                                        <a href=${menu.url} class="mx-1" >
                                            <span class="w-8 fas ${menu.iconName} p-2 bg-gray-200 dark:text-white dark:bg-gray-600 rounded-full text-center"></span>
                                            ${menu.label}
                                        </a>
                                    `}
                                </li>
                            `
                        )}
                        <!-- end menus -->
                    </ul>
                </div>
            </div>
        </nav>
        `;
    }
}

declare global {
    interface HTMLElementTagNameMap {
        "fmf-header": FMFHeader;
    }
}

export class HeaderItem {
    constructor(
        public label: string,
        public iconName: string,
        public url: string,
        public isHome: boolean = false,
    ) { }
}

export class HeaderMenu extends HeaderItem {
    constructor(
        label: string,
        iconName: string,
        url: string,
        isHome: boolean = false,
        public items: Array<HeaderItem> = [],
    ) {
        super(label, iconName, url, isHome);
    }

    public get hasItems(): boolean {
        return this.items.length > 0
    }
}
