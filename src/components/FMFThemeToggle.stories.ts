import { Meta, StoryObj } from "@storybook/web-components";
import { FMFThemeToggle } from "./FMFThemeToggle";
import "./FMFThemeToggle";

const meta = {
    component: "fmf-theme-toggle",
    title: "Components/Theme Toggle",
    tags: [ "autodocs" ]
} satisfies Meta<FMFThemeToggle>;
export default meta;

type Story = StoryObj;

export const Light = {
    parameters: {
        themes: {
            themeOverride: "Light"
        }
    }
} satisfies Story;

export const Dark = {
    parameters: {
        themes: {
            themeOverride: "Dark"
        }
    }
} satisfies Story;
